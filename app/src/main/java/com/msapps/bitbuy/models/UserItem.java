package com.msapps.bitbuy.models;

import android.graphics.Bitmap;

import com.google.gson.JsonArray;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.msapps.bitbuy.utils.GsonManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by msapp_Igor on 3/29/18.
 */

public class UserItem implements Serializable{

    @SerializedName("_id")
    private String _id;
    @SerializedName("userID")
    private String userID;
    @SerializedName("name")
    private String name;
    @SerializedName("price")
    private double price;
    @SerializedName("description")
    private String description;
    @SerializedName("date")
    private String date;
    @SerializedName("picture")
    @Expose
    private Picture picture;


    private String imageUrl;
    private Bitmap imageBitmap;


    public UserItem(String itemName, double itemPrice, String imageUrl) {
        this.setName(itemName);
        this.setPrice(itemPrice);
        this.setImageUrl(imageUrl);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImageUrl() {

        if (imageUrl != null) {
            return imageUrl;
        } else {
            if(picture != null) {
                return picture.getUrl();
            }

        }
        return null;
  
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    class Picture {

        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("name")
        @Expose
        private String name;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}


