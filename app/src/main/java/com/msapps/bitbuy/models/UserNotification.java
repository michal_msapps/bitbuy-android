package com.msapps.bitbuy.models;

public class UserNotification {

    private String imageURL;
    private String message;

    public UserNotification(String message){
        this.message = message;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
