package com.msapps.bitbuy.models;

import com.msapps.bitbuy.dataTypes.User;

import org.jetbrains.annotations.NotNull;

public class Candidacy extends User {


    public Candidacy(@NotNull String fName, @NotNull String email, double userRating, @NotNull String message) {
        super(fName, email, userRating, message);
    }
}
