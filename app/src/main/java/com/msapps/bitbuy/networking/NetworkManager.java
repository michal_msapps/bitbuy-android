package com.msapps.bitbuy.networking;

import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.msapps.bitbuy.interfaces.HttpResponseListener;
import com.msapps.bitbuy.utils.JsonArrayRequestManager;
import com.msapps.bitbuy.utils.MyApplication;
import com.msapps.bitbuy.utils.Statics;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by msapp_Igor on 12/25/17.
 */

public class NetworkManager {

    private static final String TAG = NetworkManager.class.getSimpleName();

   // private static final String BASE_URL = "http://10.0.0.40:3000";
    private static final String BASE_URL = "https://obscure-beach-67052.herokuapp.com";
    private static final int REFRESH_TOKEN_ERROR = 402;

    private static RequestQueue mRequestQueue;

    public NetworkManager() {
    }

    private static NetworkManager networkManagerJavaInstance = new NetworkManager();

    public static NetworkManager getInstance() {
        getRequestQueue();

        if (networkManagerJavaInstance == null) {
            return new NetworkManager();
        }
        return networkManagerJavaInstance;
    }


    private static RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(MyApplication.getMyApplicationInstance().getAppContext());
        }
        return mRequestQueue;
    }


    /**
     * Retrieve user token from FireBase
     *
     * @param onCompleteListener
     */
    public static void getUserToken(OnCompleteListener<GetTokenResult> onCompleteListener) {

        FirebaseUser fUser = FirebaseAuth.getInstance().getCurrentUser();

        if (fUser != null) {
            fUser.getIdToken(true).addOnCompleteListener(onCompleteListener);
        }

    }

    /**
     * Refreshing the Token on FireBase
     *
     * @param listener listener for the result from FireBase
     */
    private void refreshToken(OnCompleteListener<GetTokenResult> listener) {

        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();

        mUser.getIdToken(true).addOnCompleteListener(listener);


    }


    public void jsonArrayRequest(final int method, final MethodName methodName, final HttpResponseListener httpResponseListener) {
        final JsonObjectRequest request = new JsonObjectRequest(method, BASE_URL + methodName.toString(), new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                httpResponseListener.onSuccess(response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NoConnectionError) {
                    // TODO: No Internet Connection
                } else if (error instanceof TimeoutError) {
                    httpResponseListener.onFailure(error.toString());
                }
            }
        });
//        {
//            @Override
//            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
//                try {
//                Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
//                if(cacheEntry == null){
//                    cacheEntry = new Cache.Entry();
//                }
//                final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
//                long now = System.currentTimeMillis();
//                final long softExpire = now + cacheHitButRefreshed;
//                final long ttl = now + cacheExpired;
//
//                cacheEntry.data = response.data;
//                cacheEntry.softTtl = softExpire;
//                cacheEntry.ttl = ttl;
//
//                String headerValue;
//                headerValue = response.headers.get("Date");
//
//                if(headerValue != null){
//                    cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
//                }
//
//                headerValue = response.headers.get(("Last-Modified"));
//
//                if(headerValue != null){
//                    cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
//                }
//
//                cacheEntry.responseHeaders = response.headers;
//
//
//                    final String jsonString = new String(response.data,
//                            HttpHeaderParser.parseCharset(response.headers));
//
//                    return Response.success(new JSONObject(jsonString), cacheEntry);
//                } catch (UnsupportedEncodingException | JSONException e) {
//                    return Response.error(new ParseError(e));
//                }
//
//
//
//            }
//
//            @Override
//            protected void deliverResponse(JSONObject response) {
//                super.deliverResponse(response);
//            }
//
//            @Override
//            public void deliverError(VolleyError error) {
//                super.deliverError(error);
//            }
//
//            @Override
//            protected VolleyError parseNetworkError(VolleyError volleyError) {
//                return super.parseNetworkError(volleyError);
//            }
//        };
        mRequestQueue.add(request);
    }

    public void jsonArrayRequest(final int method, final JSONArray params, final MethodName methodName, final HttpResponseListener httpResponseListener) {
        getUserToken(new OnCompleteListener<GetTokenResult>() {
            @Override
            public void onComplete(@NonNull final Task<GetTokenResult> task) {
                if (task.isSuccessful()) {
                    JsonArrayRequest request = new JsonArrayRequest(method, BASE_URL + methodName.toString(), params, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            httpResponseListener.onSuccess(response.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof NoConnectionError) {
                                // TODO: No Internet Connection
                            } else if (error.networkResponse != null && error.networkResponse.statusCode == REFRESH_TOKEN_ERROR) {
                                refreshToken(new OnCompleteListener<GetTokenResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                                        String userToken = task.getResult().getToken();
                                        MyApplication.getMyApplicationInstance().getAppSharedPrefManager().savePrefString(Statics.INSTANCE.getFIREBASE_USER_TOKEN(), userToken);

                                        jsonArrayRequest(method, params, methodName, httpResponseListener);
                                    }
                                });
                            }
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            MyApplication.getMyApplicationInstance().getAppSharedPrefManager().savePrefString(Statics.INSTANCE.getFIREBASE_USER_TOKEN(), task.getResult().getToken());

                            HashMap<String, String> headers = new HashMap<>();
                            headers.put(Statics.INSTANCE.getACCESS_TOKEN_KEY(), task.getResult().getToken());

                            Log.d(TAG, "The user TOKEN is = " + task.getResult().getToken());

                            return headers;
                        }
                    };

                    mRequestQueue.add(request);
                }
            }
        });
    }

    public void jsonArrayRequest(final int method, final JSONObject params, final MethodName methodName, final HttpResponseListener httpResponseListener) {

        getUserToken(new OnCompleteListener<GetTokenResult>() {
            @Override
            public void onComplete(@NonNull final Task<GetTokenResult> task) {
                if (task.isSuccessful()) {

                    JsonArrayRequestManager request = new JsonArrayRequestManager(method, BASE_URL + methodName.toString(), params, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            httpResponseListener.onSuccess(response.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof NoConnectionError) {
                                // TODO: No Internet Connection
                            } else if (error.networkResponse != null && error.networkResponse.statusCode == REFRESH_TOKEN_ERROR) {
                                refreshToken(new OnCompleteListener<GetTokenResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                                        String userToken = task.getResult().getToken();
                                        MyApplication.getMyApplicationInstance().getAppSharedPrefManager().savePrefString(Statics.INSTANCE.getFIREBASE_USER_TOKEN(), userToken);

                                        jsonArrayRequest(method, params, methodName, httpResponseListener);
                                    }
                                });
                            }
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            MyApplication.getMyApplicationInstance().getAppSharedPrefManager().savePrefString(Statics.INSTANCE.getFIREBASE_USER_TOKEN(), task.getResult().getToken());

                            HashMap<String, String> headers = new HashMap<>();
                            headers.put(Statics.INSTANCE.getACCESS_TOKEN_KEY(), task.getResult().getToken());

                            Log.d(TAG, "The user TOKEN is = " + task.getResult().getToken());
                            return headers;
                        }
                    };

                    mRequestQueue.add(request);

                }
            }
        });


    }

    public void jsonOBJRequest(final int method, final JSONObject params, final MethodName methodName, final HttpResponseListener httpResponseListener) {

        getUserToken(new OnCompleteListener<GetTokenResult>() {
            @Override
            public void onComplete(@NonNull final Task<GetTokenResult> task) {
                if (task.isSuccessful()) {

                    JsonObjectRequest request = new JsonObjectRequest(method, BASE_URL + methodName.toString(), params, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            httpResponseListener.onSuccess(response.toString());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof NoConnectionError) {
                                // TODO: No Internet Connection
                            } else if (error instanceof TimeoutError) {
                                httpResponseListener.onFailure(error.toString());
                            } else if (error.networkResponse != null && error.networkResponse.statusCode == REFRESH_TOKEN_ERROR) {
                                refreshToken(new OnCompleteListener<GetTokenResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                                        String userToken = task.getResult().getToken();
                                        MyApplication.getMyApplicationInstance().getAppSharedPrefManager().savePrefString(Statics.INSTANCE.getFIREBASE_USER_TOKEN(), userToken);

                                        jsonOBJRequest(method, params, methodName, httpResponseListener);
                                    }
                                });
                            }
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            MyApplication.getMyApplicationInstance().getAppSharedPrefManager().savePrefString(Statics.INSTANCE.getFIREBASE_USER_TOKEN(), task.getResult().getToken());

                            HashMap<String, String> headers = new HashMap<>();
                            headers.put(Statics.INSTANCE.getACCESS_TOKEN_KEY(), task.getResult().getToken());
                            Log.d(TAG, "The user TOKEN is = " + task.getResult().getToken());
                            return headers;
                        }
                    };

                    mRequestQueue.add(request);
                }
            }
        });

    }


    public enum MethodName {
        REGISTER_USER("/userReg"),
        POST_USER_ITEM("/postItem"),
        GET_ALL_ITEMS("/getItems"),
        GET_USER_ITEMS("/getUserItems"),
        DELETE_ITEM("/deleteItem"),
        UPDATE_ITEM("/updateItem"),
        GET_USER_RATING("/getUserRatingAvg"),
        GET_USER_RATING_REVIEWS("/getUserReviews"),
        DELETE_USER("/userDelete"),
        POST_RATING("/postRating"),
        APPLY_TO_BUY("/applyToBuy"),
        GET_CANDIDATES("/getCandidates"),
        GET_CANDIDATE_STATUS("/getCandidateStatus"),
        DELETE_CANDIDACY("/deleteCandidacy"),
        SELECT_CANDIDATE("/selectCandidate");

        private final String name;

        MethodName(String name) {
            this.name = name;
        }


        @Override
        public String toString() {
            return this.name;
        }
    }


}
