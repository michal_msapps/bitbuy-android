package com.msapps.bitbuy.networking;

import com.android.volley.VolleyError;

import com.google.gson.reflect.TypeToken;
import com.msapps.bitbuy.dataTypes.ProductItem;
import com.msapps.bitbuy.interfaces.HttpResponseListener;
import com.msapps.bitbuy.utils.GsonManager;


import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

//public class ServerManager {
//
//    private static ServerManager serverManagerInstance = new ServerManager();
//
//    ServerManager() {
//    }
//
//
//    public ServerManager getInstance() {
//        if (serverManagerInstance == null) {
//            return new ServerManager();
//        }
//        return serverManagerInstance;
//
//
//    }
//
//
//    public ArrayList<ProductItem> getUserItems(HttpResponseListener httpResponseListener) {
//        NetworkAPI.getInstance().getUserItems( new HttpResponseListener() {
//
//            @Override
//            public void onFailure(@NotNull VolleyError error) {
//
//            }
//
//            @Override
//            public void onSuccess(@NotNull String result) {
//                GsonManager.INSTANCE.getInstance().fromJson(result, new TypeToken<List<ProductItem>>() {
//                }.getType());
//            }
//        });
//
//        return new ArrayList<ProductItem>();
//    }
//
//
//    public enum URLS {
//        REGISTER_USER("/userReg"),
//        POST_USER_ITEM("/postItem"),
//        GET_ALL_ITEMS("/getItems"),
//        GET_USER_ITEMS("/getUserItems"),
//        DELETE_ITEM("/deleteItem"),
//        UPDATE_ITEM("/updateItem"),
//        GET_USER_RATING_AVG("/getUserRatingAvg"),
//        GET_USER_REVIEWS("/getUserReviews"),
//        DELETE_USER("/userDelete"),
//        POST_RATING("/postRating"),;
//
//
//        private final String name;
//
//        URLS(String name) {
//            this.name = name;
//        }
//
//        private static URLS getValue(String urls) {
//            switch (urls) {
//                case "userReg":
//                    return REGISTER_USER;
//
//                case "postItem":
//                    return POST_USER_ITEM;
//                case "getItems":
//                    return GET_ALL_ITEMS;
//                case "getUserItems":
//                    return GET_USER_ITEMS;
//                case "deleteItem":
//                    return DELETE_ITEM;
//                case "updateItem":
//                    return UPDATE_ITEM;
//                case "getUserRatingAvg":
//                    return GET_USER_RATING_AVG;
//                case "getUserReviews":
//                    return GET_USER_REVIEWS;
//                case "userDelete":
//                    return DELETE_USER;
//                case "postRating":
//                    return POST_RATING;
//
//                default:
//                    return REGISTER_USER;
//            }
//        }
//
//        @Override
//        public String toString() {
//            return this.name;
//        }
//    }
//
//}
