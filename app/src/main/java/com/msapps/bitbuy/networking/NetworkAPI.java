package com.msapps.bitbuy.networking;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import com.google.gson.JsonObject;
import com.msapps.bitbuy.interfaces.HttpResponseListener;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class NetworkAPI {

    private static NetworkAPI networkAPIInstance = new NetworkAPI();


    public static NetworkAPI getInstance() {
        if (networkAPIInstance == null) {
            return new NetworkAPI();
        }
        return networkAPIInstance;
    }

    public void registerUser(HttpResponseListener listener) {
        NetworkManager.getInstance().jsonOBJRequest(Request.Method.POST, new JSONObject(), NetworkManager.MethodName.REGISTER_USER, listener);
    }


    public void postUserItem(final String title, final Double itemPrice, final String imageURL, String picName, final String description, final HttpResponseListener responseListener) {

        final JSONObject params = new JSONObject();
        final JSONObject picObj = new JSONObject();

        try {

            picObj.put("url", imageURL);
            picObj.put("name", picName);

            params.put("name", title);
            params.put("price", itemPrice);
            params.put("picture", picObj);
            params.put("description", description);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.getInstance().jsonOBJRequest(Request.Method.POST, params, NetworkManager.MethodName.POST_USER_ITEM, responseListener);
    }

    public void getAllItems(final HttpResponseListener httpResponseListener) {


        NetworkManager.getInstance().jsonArrayRequest(Request.Method.GET, NetworkManager.MethodName.GET_ALL_ITEMS, httpResponseListener);


    }

    public void getUserItems(final HttpResponseListener listener) {


        NetworkManager.getInstance().jsonArrayRequest(Request.Method.GET, new JSONArray(), NetworkManager.MethodName.GET_USER_ITEMS, listener);


    }

    public void deleteUserItem(String itemID, HttpResponseListener httpResponseListener) {

        JSONObject params = new JSONObject();

        try {
            params.put("itemID", itemID);

            NetworkManager.getInstance().jsonOBJRequest(Request.Method.POST, params, NetworkManager.MethodName.DELETE_ITEM, httpResponseListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void updateItem(final String itemID, final String name, final Double itemPrice, final String imageURL, final HttpResponseListener httpResponseListener) {

        JSONObject params = new JSONObject();

        try {
            params.put("id", itemID);
            params.put("name", name);
            params.put("price", itemPrice);
            params.put("picture", imageURL);


            NetworkManager.getInstance().jsonOBJRequest(Request.Method.POST, params, NetworkManager.MethodName.UPDATE_ITEM, httpResponseListener);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getUserRating(HttpResponseListener httpResponseListener) {

        NetworkManager.getInstance().jsonOBJRequest(Request.Method.POST, new JSONObject(), NetworkManager.MethodName.GET_USER_RATING, httpResponseListener);

    }

    public void getUserRatingReviews(String userId, HttpResponseListener httpResponseListener) {

        JSONObject params = new JSONObject();

        try {
            params.put("id", userId);
            NetworkManager.getInstance().jsonOBJRequest(Request.Method.POST, params, NetworkManager.MethodName.GET_USER_RATING_REVIEWS, httpResponseListener);
        } catch (JSONException e) {
            e.printStackTrace();

        }


    }

    public void deleteUser(HttpResponseListener httpResponseListener) {


        NetworkManager.getInstance().jsonOBJRequest(Request.Method.POST, new JSONObject(), NetworkManager.MethodName.DELETE_USER, httpResponseListener);

    }

    public void postRating(final String userID, final String reviewerUserID, final String content, final int rating, final HttpResponseListener httpResponseListener) {

        JSONObject params = new JSONObject();

        try {

            params.put("userID", userID);
            params.put("reviewerID", reviewerUserID);
            params.put("text", content);
            params.put("rating", rating);

            NetworkManager.getInstance().jsonOBJRequest(Request.Method.POST, params, NetworkManager.MethodName.POST_RATING, httpResponseListener);

        } catch (JSONException ex) {
            ex.printStackTrace();

        }


    }

    public void applyToBuy(String itemID, String userID, HttpResponseListener httpResponseListener) {

        JSONObject params = new JSONObject();


        try {
            params.put("userID", userID);
            params.put("id", itemID);

            NetworkManager.getInstance().jsonOBJRequest(Request.Method.POST, params, NetworkManager.MethodName.APPLY_TO_BUY, httpResponseListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getCandidates(String itemID) {


        JSONObject params = new JSONObject();

        try {
            params.put("id", itemID);

            NetworkManager.getInstance().jsonArrayRequest(Request.Method.POST, params, NetworkManager.MethodName.GET_CANDIDATES, new HttpResponseListener() {
                @Override
                public void onSuccess(@NotNull String result) {
                    //TODO.....
                }

                @Override
                public void onFailure(String error) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getCandidateStatus() {

        NetworkManager.getInstance().jsonArrayRequest(Request.Method.POST, new JSONArray(), NetworkManager.MethodName.GET_CANDIDATE_STATUS, new HttpResponseListener() {
            @Override
            public void onSuccess(@NotNull String result) {
                //TODO......
            }

            @Override
            public void onFailure(String error) {

            }
        });
    }

    public void deleteCandidacy(String itemID) {

        JSONObject params = new JSONObject();

        try {
            params.put("id", itemID);

            NetworkManager.getInstance().jsonArrayRequest(Request.Method.POST, params, NetworkManager.MethodName.DELETE_CANDIDACY, new HttpResponseListener() {
                @Override
                public void onSuccess(@NotNull String result) {
                    //TODO....
                }

                @Override
                public void onFailure(String error) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void selectCandidate() {
        NetworkManager.getInstance().jsonOBJRequest(Request.Method.POST, new JSONObject(), NetworkManager.MethodName.SELECT_CANDIDATE, new HttpResponseListener() {
            @Override
            public void onSuccess(@NotNull String result) {
                //TODO....
            }

            @Override
            public void onFailure(String error) {

            }
        });
    }

}
