package com.msapps.bitbuy.firebase;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class FireBaseStorageManager {

    private static FireBaseStorageManager instance;
    private static FirebaseStorage storage;
    private static StorageReference storageReference;

    private FireBaseStorageManager(){}


    public static FireBaseStorageManager getInstance() {

        if(instance == null){
            instance = new FireBaseStorageManager();
            storage = FirebaseStorage.getInstance();
            storageReference = storage.getReference();
        }

        return instance;
    }

    public  StorageReference getStorageReference() {
        return storageReference;
    }

    public  FirebaseStorage getStorage() {
        return storage;
    }
}
