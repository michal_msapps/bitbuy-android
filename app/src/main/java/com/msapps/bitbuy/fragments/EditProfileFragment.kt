package com.msapps.bitbuy.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.msapps.bitbuy.R

class EditProfileFragment : BaseFragment(){

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mainView = inflater!!.inflate(R.layout.fragment_edit_profile, container, false)


        return mainView
    }

}