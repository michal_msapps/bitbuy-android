package com.msapps.bitbuy.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.reflect.TypeToken;
import com.msapps.bitbuy.R;
import com.msapps.bitbuy.adapters.UserItemsAdapter;
import com.msapps.bitbuy.interfaces.HttpResponseListener;
import com.msapps.bitbuy.models.UserItem;
import com.msapps.bitbuy.networking.NetworkAPI;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by msapp_Igor on 4/3/18.
 */

public class UserItemsFragment extends BaseRefreshableFragment {

    private final String TAG = this.getClass().getSimpleName();

    private View view = getView();
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private UserItemsAdapter adapter;
    private ArrayList<UserItem> itemsArr;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeRefreshLayout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_user_items, container, false);

        viewInit();
        recyclerViewInit();

        getData(true);
        swipeRefreshLayout.setOnRefreshListener(this);

        return view;
    }


    private void viewInit() {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.fragment_user_items_swipeToContainer);
    }

    private void recyclerViewInit() {
        recyclerView = view.findViewById(R.id.user_items_RecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);


    }


    private void getData(boolean showProgressDialod) {
        if(showProgressDialod) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Loading...");
            progressDialog.show();
        }
        NetworkAPI.getInstance().getUserItems(new HttpResponseListener() {
            @Override
            public void onSuccess(@NotNull String result) {
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                try {
                    JSONArray jsonObject = new JSONArray(result);


                    Type listType = new TypeToken<List<UserItem>>() {
                    }.getType();
                    itemsArr = new Gson().fromJson(jsonObject.toString(), listType);
                    adapter = new UserItemsAdapter(itemsArr, getContext());
                    recyclerView.setAdapter(adapter);

                    Log.d(TAG, "kjhdfg");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(@NotNull String result) {
                Log.d(TAG, result);
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        getData(false);
    }
}
