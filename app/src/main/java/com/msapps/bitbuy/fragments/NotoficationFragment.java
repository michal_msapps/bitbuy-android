package com.msapps.bitbuy.fragments;

import android.app.Notification;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msapps.bitbuy.R;
import com.msapps.bitbuy.adapters.NotificationsAdapter;
import com.msapps.bitbuy.models.UserNotification;

import java.util.ArrayList;


/**
 * Created by msapp_Igor on 4/12/18.
 */

public class NotoficationFragment extends BaseRefreshableFragment {


    private View view = getView();
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private NotificationsAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_notifications, container, false);
        setRecyclerview();



        return view;

    }



    private void setRecyclerview(){
        recyclerView = view.findViewById(R.id.notifications_RecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);

        adapter = new NotificationsAdapter(getArrData(), getContext());

        recyclerView.setAdapter(adapter);
    }

    private ArrayList<UserNotification> getArrData(){

        ArrayList<UserNotification> notifications = new ArrayList<>();

        for (int i = 0; i < 30; i++) {
            notifications.add(new UserNotification("New Message from " + i));
        }

        return notifications;
    }



}
