package com.msapps.bitbuy.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.OnProgressListener;

import com.google.firebase.storage.UploadTask;
import com.msapps.bitbuy.R;
import com.msapps.bitbuy.activities.MainActivity;
import com.msapps.bitbuy.firebase.FireBaseStorageManager;
import com.msapps.bitbuy.interfaces.HttpResponseListener;
import com.msapps.bitbuy.networking.NetworkAPI;
import com.msapps.bitbuy.utils.ImagePicker;
import com.msapps.bitbuy.utils.MyApplication;


import org.jetbrains.annotations.NotNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

/**
 * Created by msapp_Igor on 4/11/18.
 */

public class AddEditFragment extends BaseFragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    public static final int PICK_IMAGE_ID = 234;
    private static final String TAG = AddEditFragment.class.getSimpleName();


    private View view;
    private EditText fragment_edit_add_itemName_ED, fragment_edit_add_link_ED, fragment_edit_add_price_ED, fragment_edit_add_info_ED;
    private Spinner fragment_edit_add_price_spinner;
    private Button fragment_submit_BTN;
    private TextView addImageIMG;

    private Uri imageUri;
    private Uri firebaseImageUrl;
    private ProgressDialog progressDialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_add_edit_item, container, false);

        viewInit();
        setSpinner();
        setOnClicks();


        return view;

    }


    /**
     * Initialize View Elements
     */
    private void viewInit() {
        addImageIMG = (TextView) view.findViewById(R.id.addImage_ImageView);
        fragment_submit_BTN = (Button) view.findViewById(R.id.fragment_submit_BTN);

        fragment_edit_add_itemName_ED = (EditText) view.findViewById(R.id.fragment_edit_add_itemName_ED);
        fragment_edit_add_link_ED = (EditText) view.findViewById(R.id.fragment_edit_add_link_ED);
        fragment_edit_add_price_ED = (EditText) view.findViewById(R.id.fragment_edit_add_price_ED);
        fragment_edit_add_info_ED = (EditText) view.findViewById(R.id.fragment_edit_add_info_ED);

        fragment_edit_add_price_spinner = (Spinner) view.findViewById(R.id.fragment_edit_add_price_spinner);

    }

    /**
     * Initializing and setting the Spinner
     */
    private void setSpinner() {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.currencyArray));
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        fragment_edit_add_price_spinner.setAdapter(arrayAdapter);

        setSpinnerTextColor();
    }

    /**
     * Changing the Spinner text color
     */
    private void setSpinnerTextColor() {
        fragment_edit_add_price_spinner.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                TextView textView = (TextView) fragment_edit_add_price_spinner.getSelectedView();
                textView.setTextColor(MyApplication.getMyApplicationInstance().getAppContext().getResources().getColor(R.color.color_add_text_color));

            }
        });
    }


    /**
     * Settings onClick on elements in Fragment
     */
    private void setOnClicks() {
        fragment_edit_add_price_spinner.setOnItemSelectedListener(this);
        addImageIMG.setOnClickListener(this);

        fragment_submit_BTN.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        fragment_edit_add_price_spinner.setSelection(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null && data.getData() != null) {

            imageUri = data.getData();

            switch (requestCode) {
                case PICK_IMAGE_ID:
                    Bitmap bitmap = ImagePicker.getImageFromResult(getContext(), resultCode, data);

                    if (bitmap != null) {

                        addImageIMG.setBackground(new BitmapDrawable(getResources(), bitmap));
                        addImageIMG.setText("");


                    }


                default:
                    super.onActivityResult(requestCode, resultCode, data);
            }
        }


    }


    /**
     * Uploading Image to FireBase Storage
     */
    private void sendToFireBaseStorage() {
        //TODO... set loader
        if (imageUri != null) {

            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            //TODO.. add userID from fireBase
            FireBaseStorageManager.getInstance().getStorageReference().child("user/" + FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    firebaseImageUrl = taskSnapshot.getDownloadUrl();

                    String picName = taskSnapshot.getMetadata().getName() + ".jpg";

                    sendItemToServer(picName);

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //TODO.. Handle Error....
                    Log.d(TAG, "The upload faild = " + e.getMessage());
                    progressDialog.dismiss();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Uploaded " + (int) progress + "%");

                }
            });

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addImage_ImageView:
                getImageIntent();
                break;
            case R.id.fragment_submit_BTN:
                //TODO.. upload the pic to Storage

                MyApplication.getMyApplicationInstance().hideKeyboard(getContext());
                sendToFireBaseStorage();
                break;
        }
    }


    private void sendItemToServer(String picName) {


        if (!firebaseImageUrl.toString().isEmpty() && !fragment_edit_add_itemName_ED.getText().toString().trim().isEmpty() &&
                isValidURL(fragment_edit_add_link_ED.getText().toString().trim()) &&
                !fragment_edit_add_price_ED.getText().toString().trim().isEmpty() && !fragment_edit_add_info_ED.getText().toString().trim().isEmpty()) {


            NetworkAPI.getInstance().postUserItem(fragment_edit_add_itemName_ED.getText().toString().trim(),
                    Double.parseDouble(fragment_edit_add_price_ED.getText().toString().trim()),
                    firebaseImageUrl.toString(), picName, fragment_edit_add_info_ED.getText().toString().trim(), new HttpResponseListener() {
                        @Override
                        public void onSuccess(@NotNull String result) {
                            Log.d(TAG, result);
                            progressDialog.dismiss();
                            ((MainActivity) getContext()).toMainFragment();
                        }

                        @Override
                        public void onFailure(@NotNull String error) {
                            Log.d(TAG, error);
                            progressDialog.dismiss();
                        }
                    }

            );
        }


    }


    private boolean isValidURL(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }

    /**
     * Creating an Intent for Pick Image
     */
    private void getImageIntent() {
        Intent getImageIntent = ImagePicker.getPickImageIntent(getContext());
        startActivityForResult(getImageIntent, PICK_IMAGE_ID);
    }
}
