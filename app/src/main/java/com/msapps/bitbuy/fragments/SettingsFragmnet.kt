package com.msapps.bitbuy.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.msapps.bitbuy.R
import com.msapps.bitbuy.adapters.SettingsAdapter


class SettingsFragmnet : BaseFragment() {

    lateinit var settingArr: ArrayList<ProfileFrgment.ProfileMenuItem>
    lateinit var recyclerView: RecyclerView
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: SettingsAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        mainView = inflater.inflate(R.layout.fragment_settings, container, false)

        recyclerViewInit()


        return mainView
    }


    /**
     * Creating RecyclerView
     */
    fun recyclerViewInit() {
        recyclerView = mainView.findViewById(R.id.settings_RecyclerView)
        layoutManager = LinearLayoutManager(context)

        recyclerView.layoutManager = layoutManager



        adapter = SettingsAdapter(context)

        recyclerView.adapter = adapter


    }


}