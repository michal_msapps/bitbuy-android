package com.msapps.bitbuy.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.msapps.bitbuy.R
import com.msapps.bitbuy.enums.FragmentName
import com.msapps.bitbuy.models.UserItem

class ItemFragment : BaseFragment(){

    lateinit var userItem : UserItem
    lateinit var itemImage : ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {

        mainView = inflater.inflate(R.layout.fragment_item_view, container, false)

        userItem = arguments.getSerializable(FragmentName.ITEM_FRAGMENT.toString()) as UserItem

        viewInit()

        Glide.with(context)
                .load(userItem.imageUrl!!)
                .into(itemImage)
 

        return mainView
    }

    private fun viewInit() {
        itemImage =  mainView.findViewById(R.id.fragment_item_view_Item_Image)
    }


    fun intiItem(userItem : UserItem){
        this.userItem = userItem
    }
}