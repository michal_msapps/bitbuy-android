package com.msapps.bitbuy.fragments

import android.app.Activity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ListView
import android.widget.TextView
import com.firebase.ui.database.FirebaseListAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.msapps.bitbuy.R
import com.msapps.bitbuy.models.ChatMessage


class KotlinChatFragment() : BaseFragment(){

    private var adapter: FirebaseListAdapter<ChatMessage>? = null
    var sendButton : FloatingActionButton? = null

    var SIGN_IN_REQUEST_CODE = 1234

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        mainView = inflater.inflate(R.layout.fragment_chat, container, false)

        displayChatMessages();

        sendButton = mainView.findViewById(R.id.fab) as FloatingActionButton

        sendButton!!.setOnClickListener {
            val input = mainView.findViewById(R.id.input) as EditText

            // Read the input field and push a new instance
            // of ChatMessage to the Firebase database
            FirebaseDatabase.getInstance()
                    .reference
                    .push()
                    .setValue(ChatMessage(input.text.toString(),
                            FirebaseAuth.getInstance()
                                    .currentUser!!
                                    .displayName)
                    )

            // Clear the input
            input.setText("")
        }

        return mainView
    }

    private fun displayChatMessages() {
        val listOfMessages = mainView.findViewById(R.id.list_of_messages) as ListView

        adapter = object : FirebaseListAdapter<ChatMessage>(context as Activity, ChatMessage::class.java,
                R.layout.message, FirebaseDatabase.getInstance().reference) {
            override fun populateView(v: View, model: ChatMessage, position: Int) {
                // Get references to the views of message.xml
                val messageText = v.findViewById<View>(R.id.message_text) as TextView
                val messageUser = v.findViewById<View>(R.id.message_user) as TextView
                val messageTime = v.findViewById<View>(R.id.message_time) as TextView

                // Set their text
                messageText.text = model.messageText
                messageUser.text = model.messageUser

            }
        }

        listOfMessages.setAdapter(adapter)
    }


}