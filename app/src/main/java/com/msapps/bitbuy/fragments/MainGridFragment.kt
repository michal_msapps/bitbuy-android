package com.msapps.bitbuy.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import com.msapps.bitbuy.R
import com.msapps.bitbuy.activities.MainActivity
import com.msapps.bitbuy.adapters.MainGridAdapter
import com.msapps.bitbuy.enums.FragmentName
import com.msapps.bitbuy.interfaces.HttpResponseListener
import com.msapps.bitbuy.interfaces.OnItemClicked
import com.msapps.bitbuy.models.UserItem
import com.msapps.bitbuy.networking.NetworkAPI
import org.json.JSONObject
import kotlin.collections.ArrayList


class MainGridFragment : BaseRefreshableFragment(), OnItemClicked {



    val TAG: String = "MainGridFragment"

    private lateinit var contex: Context
    private lateinit var fragment_main_grid_swipeToContainer: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var dividerItemDecoration: DividerItemDecoration
    private lateinit var adapter: MainGridAdapter
    @SerializedName("ProductItem")
    private var userItemsArr: ArrayList<UserItem> = ArrayList()
    var count = 0


    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)

        contex = activity!!
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        mainView = inflater!!.inflate(R.layout.fragment_main_grid_view, container, false)

        //TODO.. if needed, set the title bar
        // (contex as MainActivity2)!!.supportActionBar!!.setTitle()

        viewInit()
//        userItemsArr =  getData()

        getData()

        autoRefresh()


        fragment_main_grid_swipeToContainer!!.setOnRefreshListener(this)

        setRefreshingColors()
        return mainView!!
    }


    //Setting the Refreshing color
    private fun setRefreshingColors() {
        fragment_main_grid_swipeToContainer!!.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark)
    }


    //Init the View elements
    fun viewInit() {
        recyclerView = mainView!!.findViewById(R.id.fragment_main_grid_RecyclerView)
        fragment_main_grid_swipeToContainer = mainView!!.findViewById(R.id.fragment_main_grid_swipeToContainer)
    }


    //Initialize the RecyclerView elements
    fun recyclerViewInit() {
        recyclerView!!.isNestedScrollingEnabled = true
        recyclerView!!.setHasFixedSize(true)
        gridLayoutManager = GridLayoutManager(contex!!, 2) //, GridLayoutManager.VERTICAL, true)


        recyclerView!!.layoutManager = gridLayoutManager!!
        //TODO..  dividerItemDecoration
        //   dividerItemDecoration = DividerItemDecoration(contex!!, RelativeLayout.)


        adapter = userItemsArr?.let { MainGridAdapter(userItemsArr, this.contex!!, this) }

        recyclerView.adapter = adapter


    }


    override fun onRefresh() {
        Log.d(TAG, "OnRefresh")
        getData()

        count++


    }

    fun getData() {
        NetworkAPI.getInstance().getAllItems(object : HttpResponseListener {
            override fun onSuccess(result: String) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.


                val gsonObj = JSONObject(result)

                val turnsType = object : TypeToken<List<UserItem>>() {}.type

                userItemsArr = Gson().fromJson<List<UserItem>>(gsonObj.getJSONArray("docs").toString(), turnsType) as ArrayList<UserItem>


                recyclerViewInit()

                //Stop refreshing layout
                fragment_main_grid_swipeToContainer.isRefreshing = false

            }


            override fun onFailure(result: String) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

                //Stop refreshing layout
                fragment_main_grid_swipeToContainer.isRefreshing = false
                Log.d(TAG, "dlksjv")
            }
        })


    }

    override fun onItemClicked(itemPos: Int) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

//        val itemFragment = ItemFragment()
//        itemFragment.intiItem(userItemsArr.get(itemPos))

       // (contex as MainActivity).transactFragment(ItemFragment(), FragmentName.ITEM_FRAGMENT.toString(), false)

        var bundle = Bundle()
        bundle.putSerializable(FragmentName.ITEM_FRAGMENT.toString(), userItemsArr[itemPos])

        var itemFradgemt = ItemFragment()
        itemFradgemt.arguments = bundle

        (contex as MainActivity).toItemFragment(itemFradgemt)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }


    /**
     * AutoRefresh - get data from server
     */
    fun autoRefresh() {
        Handler().postDelayed({
            //   userItemsArr = getData()
        }, 30000)
    }


    /**
     * Receiving All Deals from DataBase
     */


//    fun getData(): ArrayList<ProductItem> {
//
//        if(userItemsArr == null){
//            userItemsArr = ArrayList()
//        }
//
//        for (i in 1..30){
//            userItemsArr.add(ProductItem("Item number ${i}",  "http", i.toDouble()))
//        }
//
//        return userItemsArr
//
//    }


//    fun getData(){
//        NetworkAPI.getInstance().getAllItems(object : HttpResponseListener{
//            override fun onAllItemsReceived(result: ArrayList<UserItem>) {
//               userItemsArr = result
//            }
//        })
//    }


}

