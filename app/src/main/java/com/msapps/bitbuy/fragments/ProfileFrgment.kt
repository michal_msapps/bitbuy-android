package com.msapps.bitbuy.fragments

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.msapps.bitbuy.R
import com.msapps.bitbuy.adapters.ProfileAdapter

class ProfileFrgment : BaseFragment() {


    lateinit var menuArr: ArrayList<ProfileMenuItem>
    lateinit var recyclerView: RecyclerView
    lateinit var layoutManager: LinearLayoutManager
    lateinit var profileAdapter: ProfileAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {

        mainView = inflater.inflate(R.layout.fragment_profile, container, false)



        recyclerViewInit()
        return mainView
    }


    fun viewInit() {

    }

    /**
     * Creating RecyclerView
     */
    fun recyclerViewInit() {
        recyclerView = mainView.findViewById(R.id.profile_RecyclerView)
        layoutManager = LinearLayoutManager(context)

        recyclerView.layoutManager = layoutManager

        menuArr = getMenus()

        profileAdapter = ProfileAdapter(context, menuArr)

        recyclerView.adapter = profileAdapter


    }


    /**
     * Evaluate ArrayList of ProfileMenuItem
     */
    fun getMenus(): ArrayList<ProfileMenuItem> {

        val arr: ArrayList<ProfileMenuItem> = ArrayList()

        for (i in 0..ProfileMenuNames.values().size - 1) {


            when (ProfileMenuNames.values()[i]) {

                ProfileMenuNames.EDIT_PROFILE -> arr.add(ProfileMenuItem(R.mipmap.icons_gender_neutral_user, ProfileMenuNames.EDIT_PROFILE))
                ProfileMenuNames.MY_ITEMS -> arr.add(ProfileMenuItem(R.mipmap.icons_list, ProfileMenuNames.MY_ITEMS))
                ProfileMenuNames.MY_CANDIDACY -> arr.add(ProfileMenuItem(R.mipmap.icons_shopping_cart, ProfileMenuNames.MY_CANDIDACY))
                ProfileMenuNames.SETTINGS -> arr.add(ProfileMenuItem(R.mipmap.icons_settings, ProfileMenuNames.SETTINGS))
                ProfileMenuNames.LOGOUT -> arr.add(ProfileMenuItem(R.mipmap.icons_exit, ProfileMenuNames.LOGOUT))

            }
        }

        return arr

    }

    enum class ProfileMenuNames(val nemuName: String) {
        EDIT_PROFILE("Edit Profile"),
        MY_ITEMS("My Items"),
        MY_CANDIDACY("My Candidacy"),
        SETTINGS("Settings"),
        LOGOUT("Logout");


        override fun toString(): String {
            return name
        }

        companion object {

            fun geFragment(fName: ProfileMenuNames): Fragment? {


                return when (fName) {
                    EDIT_PROFILE -> EditProfileFragment()
                    MY_ITEMS -> UserItemsFragment()
                    MY_CANDIDACY -> MyCandidacyFragment()
                    SETTINGS -> SettingsFragmnet()
                    LOGOUT -> null
                    else -> null
                }


            }

            fun getProfileMenuNames( name: String) : ProfileMenuNames?{
                return when(name){
                    "Edit Profile" -> EDIT_PROFILE
                    "My Items" -> MY_ITEMS
                    "My Candidacy" -> MY_CANDIDACY
                    "Settings" -> SETTINGS
                    "Logout" -> LOGOUT

                    else -> null
                }
            }


        }


    }

    inner class ProfileMenuItem(icon: Int, name: ProfileMenuNames) {
        var icon = icon
        var name = name.nemuName

    }

}