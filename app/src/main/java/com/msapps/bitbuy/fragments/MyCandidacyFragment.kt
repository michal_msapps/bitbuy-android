package com.msapps.bitbuy.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.msapps.bitbuy.R
import com.msapps.bitbuy.adapters.CandidacyAdapter
import com.msapps.bitbuy.models.Candidacy
import com.msapps.bitbuy.models.UserItem

public class MyCandidacyFragment : BaseRefreshableFragment() {

    lateinit var recyclerView: RecyclerView
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: CandidacyAdapter



    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        mainView = inflater!!.inflate(R.layout.fragmnet_candidacy, container, false)

        setRecyclerView()


        return mainView
    }


    fun setRecyclerView(){
        recyclerView = mainView.findViewById(R.id.user_candidacy_RecyclerView)
        recyclerView.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(context)

        recyclerView.layoutManager = layoutManager

        adapter = CandidacyAdapter(getCandidacy(), context)

        recyclerView.adapter = adapter
    }

    fun getCandidacy() : ArrayList<Candidacy>{

      var candidacyArr = ArrayList<Candidacy>()

        for (i in 0..30){
            var candedacy = Candidacy("UseName " + i, "email@me.com", i.toDouble(), "kzdjfgvjksdgv")
            candedacy.item = UserItem("ItemName " + i, i.toDouble(), "jjjj")

            candidacyArr.add(candedacy)
        }

        return candidacyArr
    }

}