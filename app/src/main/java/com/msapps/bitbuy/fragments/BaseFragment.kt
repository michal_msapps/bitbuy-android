package com.msapps.bitbuy.fragments

import android.app.Fragment
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.View
import com.msapps.bitbuy.activities.MainActivity
import com.msapps.bitbuy.dataTypes.ProductItem

/**
 * Created by msapp_Igor on 3/29/18.
 */
open class BaseFragment : Fragment() {

    lateinit var mainActivity: MainActivity
    lateinit var mainView: View

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity = activity as MainActivity
    }

    override fun getContext(): Context {
        return if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            super.getContext()
        }else{
            super.getActivity()
        }
    }


//    open fun getData(): ArrayList<ProductItem> {
//
//        var userItemsArr = java.util.ArrayList<ProductItem>()
//
//        if (userItemsArr == null) {
//            userItemsArr = ArrayList()
//        }
//
//        for (i in 1..30) {
//            userItemsArr.add(ProductItem("Item number ${i}", "http", i.toDouble()))
//        }
//
//        return userItemsArr
//
//    }


}