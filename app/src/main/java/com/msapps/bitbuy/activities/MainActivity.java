package com.msapps.bitbuy.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.transition.Slide;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.msapps.bitbuy.R;
import com.msapps.bitbuy.enums.FragmentName;
import com.msapps.bitbuy.fragments.AddEditFragment;
import com.msapps.bitbuy.fragments.MainGridFragment;
import com.msapps.bitbuy.fragments.MyCandidacyFragment;
import com.msapps.bitbuy.fragments.NotoficationFragment;
import com.msapps.bitbuy.fragments.ProfileFrgment;
import com.msapps.bitbuy.fragments.UserItemsFragment;
import com.msapps.bitbuy.interfaces.OnAdapterItemClickedListener;
import com.msapps.bitbuy.view.BottomNavigationViewHelper;

/**
 * Created by msapp_Igor on 4/2/18.
 */

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener, OnAdapterItemClickedListener {


    private BottomNavigationView bottomNavigationView;

    private TextView toolBarTitle;
    private ImageView toolBarIcon;
    private Toolbar toolbar;

    private boolean isSafeToCommitFragment;


    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewInit();
        onClicksInit();
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        setFragmentTransaction(FragmentName.MAIN_GRID_FRAGMENT, false);
        setUserIconOnToolBar();


    }

    private void viewInit() {
        bottomNavigationView = findViewById(R.id.main_bottomNavigationView);
        toolbar = (Toolbar) findViewById(R.id.toolBar);
        toolBarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolBarIcon = (ImageView) findViewById(R.id.toolbar_icon);
    }

    private void onClicksInit() {
        toolBarIcon.setOnClickListener(this);
    }


    private void setFragmentTransaction(Fragment fragment, FragmentName tagName,  boolean addToBackStack){
        transactFragment(fragment, tagName.toString(), addToBackStack);
    }

    /**
     * Preparing for fragment transaction
     *
     * @param tagName
     * @param addToBackStack
     */
    private void setFragmentTransaction(FragmentName tagName, boolean addToBackStack) {

        Fragment transactionFragment = FragmentName.getFragment(tagName);

        if (transactionFragment != null) {
            transactFragment(transactionFragment, tagName.toString(), addToBackStack);
        }
    }

    /**
     * Preparing for fragment transaction on Profile
     *
     * @param profileMenuNames
     * @param addToBackStack
     */
    private void setFragmentTransaction(ProfileFrgment.ProfileMenuNames profileMenuNames, boolean addToBackStack) {


        switch (profileMenuNames) {
            case EDIT_PROFILE:
                toolBarIcon.setVisibility(View.GONE);
                break;
            case MY_ITEMS:
                popupBackStack();
                if (bottomNavigationView.getVisibility() == View.GONE) {
                    bottomNavigationView.setVisibility(View.VISIBLE);
                }
                break;
            case MY_CANDIDACY:
                popupBackStack();
                if (bottomNavigationView.getVisibility() == View.GONE) {
                    bottomNavigationView.setVisibility(View.VISIBLE);
                }
                break;
            case SETTINGS:
                break;
            case LOGOUT:
                break;
        }


        Fragment transactionFragment = ProfileFrgment.ProfileMenuNames.Companion.geFragment(profileMenuNames);

        if (transactionFragment != null) {
            transactFragment(transactionFragment, profileMenuNames.getNemuName(), addToBackStack);
        }


    }

    /**
     * Starting a fragment transaction
     *
     * @param fragment
     * @param tagName
     * @param addToBackStack
     */
    public void transactFragment(Fragment fragment, String tagName, boolean addToBackStack) {


        fragmentTransaction = getFragmentManager().beginTransaction();

        if (tagName == FragmentName.PROFILE_FRAGMENT.toString()) {
            //TODO... fragment animation
//            fragmentTransaction.setCustomAnimations(R.animator.slide_from_right, R.animator.slide_from_left, R.animator.slide_from_left, R.animator.slide_from_right);
        }

        fragmentTransaction.replace(R.id.activity_main_container, fragment, tagName.toString());


        if (addToBackStack) {
            fragmentTransaction.addToBackStack(tagName.toString());
        }


        if(isSafeToCommitFragment) {
            fragmentTransaction.commitAllowingStateLoss();
        }else {
            fragmentTransaction.commit();
        }

//        getFragmentManager().executePendingTransactions();



        setToolbarTitle(tagName.toString());
    }



    @Override
    public void onBackPressed() {
        if (getFragmentManager().findFragmentByTag(FragmentName.PROFILE_FRAGMENT.toString()) != null) {
            bottomNavigationView.setVisibility(View.VISIBLE);
        }
        super.onBackPressed();

        if (toolBarIcon.getVisibility() == View.GONE || toolBarIcon.getVisibility() == View.INVISIBLE) {
            toolBarIcon.setVisibility(View.VISIBLE);
        }

        //Set toolbarTitle onBackPressed
        setToolbarTitle(getFragmentManager().findFragmentById(R.id.activity_main_container).getTag());

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.navigation_search:
                setFragmentTransaction(FragmentName.MAIN_GRID_FRAGMENT, false);
                return true;
            case R.id.navigation_my_items:
                setFragmentTransaction(FragmentName.USER_ITEM_FRAGMENT, false);
                return true;
            case R.id.navigation_plus:
                setFragmentTransaction(FragmentName.ADD_ITEM_FRAGMENT, false);
                return true;
            case R.id.navigation_notifications:
                setFragmentTransaction(FragmentName.NOTIFICATION_FRAGMENT, false);
                return true;
            case R.id.navigation_shopping_card:
                setFragmentTransaction(FragmentName.CANDIDACY_FRAGMENT, false);
                return true;
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_icon:
                if (getFragmentManager().findFragmentByTag(FragmentName.PROFILE_FRAGMENT.toString()) == null) {
                    setFragmentTransaction(FragmentName.PROFILE_FRAGMENT, true);
                    bottomNavigationView.setVisibility(View.GONE);
                }
                break;

        }
    }

    public void toMainFragment() {
        setFragmentTransaction(FragmentName.MAIN_GRID_FRAGMENT, false);
    }

    public void toItemFragment(Fragment fragment){
        setFragmentTransaction(fragment, FragmentName.ITEM_FRAGMENT , false);
    }


    private void popupBackStack() {
        getFragmentManager().popBackStack();
    }


    public void setToolbarTitle(String title) {
        toolBarTitle.setText(title);
    }

    public void setToolBarIcon() {
        //TODO... set toolbar image when logged in
    }


    /**
     * Setting the user profile picture
     */
    private void setUserIconOnToolBar() {

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {

            RequestOptions requestOptions = new RequestOptions();
            requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(50));

            Glide.with(this)
                    .load(FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl())
                    .apply(requestOptions)
                    .into(toolBarIcon);
        }
    }


    @Override
    public void onAdapterItemClicked(String str) {
        setFragmentTransaction(ProfileFrgment.ProfileMenuNames.Companion.getProfileMenuNames(str), false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isSafeToCommitFragment = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isSafeToCommitFragment = false;
    }
}
