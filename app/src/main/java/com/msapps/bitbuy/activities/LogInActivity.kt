package com.msapps.bitbuy.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.android.volley.VolleyError
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.msapps.bitbuy.R
import com.msapps.bitbuy.interfaces.HttpResponseListener
import com.msapps.bitbuy.networking.NetworkAPI
import org.json.JSONObject
import java.util.*

@Suppress("UNREACHABLE_CODE")
/**
 * Created by msapp_Igor on 12/19/17.
 */


class LogInActivity : AppCompatActivity(), View.OnClickListener {


    //Declaring Constance
    companion object {
        const val RC_SIGN_IN = 984
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)

        setFireBaseAoutProviders()
       // setOnCkick()


    }

//    private fun setOnCkick() {
//        logOut_BTN.setOnClickListener(this)
//        deleteUser_BTN.setOnClickListener(this)
//    }


    /**
     * Setting the authentication providers
     */
    fun setFireBaseAoutProviders() {

        var providers = Arrays.asList(AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(), AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build()
               )

        startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().setAvailableProviders(providers).build(), RC_SIGN_IN)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode.equals(RC_SIGN_IN)) {
            val response: IdpResponse = IdpResponse.fromResultIntent(data)!!

            if (resultCode.equals(Activity.RESULT_OK)) {
                //Successfully signed in
                val user = FirebaseAuth.getInstance().currentUser


                sedingUserCredToDataBase(user!!)


            } else {
                // Sign in failed, check response for error code
                setFireBaseAoutProviders()
            }
        }

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
        //    logOut_BTN.id -> signOut()
         //   deleteUser_BTN.id -> deleteUserAcount()
        }
    }


    fun signOut() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(OnCompleteListener { result ->
                    //
                    if (result.isSuccessful) {

                    }
                })
    }

    fun deleteUserAcount() {
        AuthUI.getInstance().delete(this)
                .addOnCompleteListener(OnCompleteListener {
                    ///
                })
    }

    fun sedingUserCredToDataBase(user: FirebaseUser) {
        var jsonOBJ: JSONObject = JSONObject()

        jsonOBJ.put("providerID", user.providerId)

        NetworkAPI.getInstance().registerUser(object : HttpResponseListener{
            override fun onSuccess(result: String) {
                Toast.makeText(applicationContext, result, Toast.LENGTH_LONG)
                toNextActivity()
            }

            override fun onFailure(result: String) {
                Toast.makeText(applicationContext, result.toString(), Toast.LENGTH_LONG)
                toNextActivity()
            }
        })

    }

    fun toNextActivity() {

        startActivity(Intent(this, MainActivity::class.java))
    }


}