package com.msapps.bitbuy.interfaces;

public interface OnAdapterItemClickedListener {
    void onAdapterItemClicked(String str);
}
