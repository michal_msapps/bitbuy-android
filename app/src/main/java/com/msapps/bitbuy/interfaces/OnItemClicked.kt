package com.msapps.bitbuy.interfaces

interface OnItemClicked {
    fun onItemClicked(itemPos : Int)
}