package com.msapps.bitbuy.interfaces

import com.android.volley.VolleyError

/**
 * Created by msapp_Igor on 12/20/17.
 */


interface HttpResponseListener {
    fun onSuccess(result: String)
    fun onFailure(result: String)
   // fun onAllItemsReceived(result: ArrayList<UserItem>)
}