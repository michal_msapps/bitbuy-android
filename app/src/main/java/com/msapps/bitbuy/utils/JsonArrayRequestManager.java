package com.msapps.bitbuy.utils;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;

import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class JsonArrayRequestManager extends JsonRequest<JSONArray> {


    public JsonArrayRequestManager(int method, String url, JSONObject jsonObject, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
        super(method, url, (jsonObject == null) ? null : jsonObject.toString(), listener, errorListener);
    }


    @Override
    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
        return null;
    }
}
