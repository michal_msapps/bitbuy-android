package com.msapps.bitbuy.utils

/**
 * Created by msapp_Igor on 12/25/17.
 */
object Statics{
    val ACCESS_TOKEN_KEY = "x-auth"
    val FIREBASE_USER_TOKEN = "firebase_user_token"
    val APP_SHARED_PREF_NAME = "app_shared_pref_name"

    val URL :String = if(MyApplication.getMyApplicationInstance().isDebug) " " else "http://10.0.0.40:3000"



}