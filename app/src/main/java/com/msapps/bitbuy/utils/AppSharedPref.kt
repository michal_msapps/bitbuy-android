package com.msapps.bitbuy.utils

import android.content.Context
import android.content.SharedPreferences
import android.support.annotation.Nullable

object AppSharedPrefManager {

    var settings: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null


    operator fun invoke(context: Context): AppSharedPrefManager {

        settings = context.getSharedPreferences(Statics.APP_SHARED_PREF_NAME, Context.MODE_PRIVATE)
        editor = settings!!.edit()

        return this
    }


    fun savePrefString(key: String, value: String) {

        editor!!.putString(key, value)

    }

    fun getPrefString(key : String , @Nullable key2 : String) : String{

        if(key2 != null){
            return settings!!.getString(key, key2)
        }else{
            return settings!!.getString(key, "")
        }
    }

    fun savePrefInt(key : String, value : Int){

        editor!!.putInt(key, value)
    }

    fun getPrefInt(key: String) : Int{

        return settings!!.getInt(key, -1)
    }


    fun setPrefLong(key : String, value: Long){

        editor!!.putLong(key, value)
    }


    fun getPrefLong(key: String) : Long{
        return settings!!.getLong(key, -1)
    }

    fun setPrefFloat(key: String, value: Float){
        editor!!.putFloat(key, value)
    }

    fun getPrefFload(key: String) : Float{

        return settings!!.getFloat(key, -1F)
    }








}