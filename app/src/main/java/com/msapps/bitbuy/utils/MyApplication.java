package com.msapps.bitbuy.utils;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.view.inputmethod.InputMethodManager;

import com.msapps.bitbuy.dataTypes.User;

/**
 * Created by msapp_Igor on 12/25/17.
 */

public class MyApplication extends Application {

    private static MyApplication myApplicationInstance;
    private Context context;
    private AppSharedPrefManager appSharedPrefManager;

    public boolean isDebug = true;

    public MyApplication() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        myApplicationInstance = this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public static MyApplication getMyApplicationInstance() {
        if (myApplicationInstance == null) {
            return new MyApplication();
        }
        return myApplicationInstance;
    }


    public Context getAppContext() {
        return context;
    }

    public AppSharedPrefManager getAppSharedPrefManager() {
        return AppSharedPrefManager.INSTANCE.invoke(getApplicationContext());
    }

    public void hideKeyboard(Context context) {
        final InputMethodManager imm = (InputMethodManager) context.getSystemService("input_method");
        imm.toggleSoftInput(1, 0);

    }

}
