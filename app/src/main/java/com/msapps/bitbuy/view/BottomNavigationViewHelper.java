package com.msapps.bitbuy.view;

import android.annotation.SuppressLint;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;

import java.lang.reflect.Field;

/**
 * Created by msapp_Igor on 4/3/18.
 */


/**
 * Removing titles from BottomNavigationView
 */
public class BottomNavigationViewHelper {
    @SuppressLint("RestrictedApi")
    public static void disableShiftMode(BottomNavigationView view){

        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);


        try {
            Field shifttingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shifttingMode.setAccessible(true);
            shifttingMode.setBoolean(menuView, false);
            shifttingMode.setAccessible(false);

            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShiftingMode(false);
                item.setPadding(0, 15, 0, 0);
                // set once again checked value, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());
            }

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
