package com.msapps.bitbuy.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.msapps.bitbuy.R
import com.msapps.bitbuy.models.UserNotification


class NotificationsAdapter(val notificationsArr: ArrayList<UserNotification>, val context: Context) : RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        val itemView = LayoutInflater.from(context).inflate(R.layout.single_notification_item, parent, false)

        return ViewHolder(itemView)


    }

    override fun getItemCount(): Int {
    //   TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return notificationsArr.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //     TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        val item = notificationsArr.get(position)

        holder.bindItems(item)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val imageView: ImageView = itemView.findViewById(R.id.notifications_fragment_icon)
        private val message: TextView = itemView.findViewById(R.id.notifications_fragment_message_TV)

        fun bindItems(notification: UserNotification) {

            //TODO... set Glide to load Image

            Glide.with(context).load(R.mipmap.splash_logo).apply(RequestOptions().circleCrop()).into(imageView)

            message.setText(notification.message)
        }
    }


}