package com.msapps.bitbuy.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SwitchCompat
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import com.msapps.bitbuy.R


class SettingsAdapter(val context: Context) : RecyclerView.Adapter<SettingsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        val itemView = LayoutInflater.from(context).inflate(R.layout.single_settings_notifications_item, parent, false)

        return ViewHolder(itemView)

    }

    override fun getItemCount(): Int {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        return SettingsNames.values().size
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        val item  = SettingsNames.values()[position]

        holder!!.switchBTN.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {

            } else {

            }

        }

        holder!!.bindItems(item)

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val title: TextView = itemView.findViewById(R.id.single_settings_title_TV)
        val switchBTN: SwitchCompat = itemView.findViewById(R.id.single_settings_switch_BTN)

        fun bindItems(settingsNames: SettingsNames) {

            title.text = settingsNames.settingsName

        }
    }


    enum class SettingsNames(val settingsName: String) {
        PUSH_NOTIFICATIONS("Push Notifications");


        override fun toString(): String {
            return name
        }


    }


}