package com.msapps.bitbuy.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.msapps.bitbuy.R
import com.msapps.bitbuy.dataTypes.ProductItem
import com.msapps.bitbuy.models.UserItem
import java.util.ArrayList


/**
 * Created by msapp_Igor on 4/3/18.
 */
class UserItemsAdapter(val itemsArr: ArrayList<UserItem>, val context: Context) : RecyclerView.Adapter<UserItemsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        val itemView = LayoutInflater.from(context).inflate(R.layout.single_user_item, parent, false)


        return ViewHolder(itemView)

    }

    override fun getItemCount(): Int {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        return itemsArr.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        val item = itemsArr[position]

        holder.bindItems(item)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val itemImage: ImageView = itemView.findViewById(R.id.user_item_imageView)
        val itemName: TextView = itemView.findViewById(R.id.user_item_name)
        val itemPrice: TextView = itemView.findViewById(R.id.user_item_price)

        fun bindItems(productItem: UserItem) {

            Glide.with(context)
                    .load(productItem.imageUrl)
                    .apply(RequestOptions()
                            .placeholder(R.mipmap.item_place_holder)
                            .transform(RoundedCorners(1)))

                    .into(itemImage)


            itemName.setText(productItem.name)
            itemPrice.setText(productItem.price.toString() + " " + context.getString(R.string.shekel))


        }
    }

}