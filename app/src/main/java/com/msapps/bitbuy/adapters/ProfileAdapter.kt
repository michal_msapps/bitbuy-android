package com.msapps.bitbuy.adapters

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.msapps.bitbuy.R
import com.msapps.bitbuy.fragments.ProfileFrgment
import com.msapps.bitbuy.interfaces.OnAdapterItemClickedListener

class ProfileAdapter(val context: Context, var menusArr: ArrayList<ProfileFrgment.ProfileMenuItem>) : RecyclerView.Adapter<ProfileAdapter.ViewHolder>(){

        val listener : OnAdapterItemClickedListener = context as OnAdapterItemClickedListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        val itemView = LayoutInflater.from(context).inflate(R.layout.single_profile_layout, parent, false)

        return ViewHolder(itemView)

    }

    override fun getItemCount(): Int {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        return menusArr.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        val item = menusArr[position]


        holder.bindItems(item)
        holder.itemView.setOnClickListener(View.OnClickListener {listener.onAdapterItemClicked(menusArr[position].name)})
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val itemText: TextView = itemView.findViewById(R.id.profile_fragment_message_TV)
        val itemImage: ImageView = itemView.findViewById(R.id.profile_fragment_icon)


        fun bindItems(item: ProfileFrgment.ProfileMenuItem) {


            //Set Opacity to 30 for all images except LogOut
            if (item.name != ProfileFrgment.ProfileMenuNames.LOGOUT.nemuName) {
                itemImage.alpha = 0.3F
            }

            Glide.with(context).load(item.icon).into(itemImage)

            itemText.setText(item.name)


        }
    }



}