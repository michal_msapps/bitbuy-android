package com.msapps.bitbuy.adapters

import android.content.Context
import android.graphics.Point
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.msapps.bitbuy.R
import com.msapps.bitbuy.interfaces.OnItemClicked
import com.msapps.bitbuy.models.UserItem


class MainGridAdapter(val itemsArr: ArrayList<UserItem>, val context: Context, val onItemClicked : OnItemClicked) : RecyclerView.Adapter<MainGridAdapter.ViewHolder>() {



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        val item = itemsArr[position]

        holder!!.bindItems(item)

        holder.itemView.setOnClickListener(View.OnClickListener { onItemClicked.onItemClicked(position) })
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainGridAdapter.ViewHolder {

        //TODO.. inflate the real Layout
        val itemView = LayoutInflater.from(context).inflate(R.layout.single_main_grid_item, parent, false)


        itemView.layoutParams.height = (getScreenWith() / 3).toInt()



        return ViewHolder(itemView)

    }

    override fun getItemCount(): Int {
        return itemsArr.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val itemName: TextView = itemView.findViewById(R.id.single_main_grid_name_TV)
        private val itemIMG: ImageView = itemView.findViewById(R.id.single_main_grid_img)
        private val price: TextView = itemView.findViewById(R.id.single_main_grid_price_TV)

        fun bindItems(productItem: UserItem) {


            itemName.setText(productItem.name)
            price.setText(productItem.price.toString() +  " " +  context.getString(R.string.shekel))



            if(productItem.imageUrl != null) {

                    Glide.with(itemIMG.context)
                            .load(productItem.imageUrl!!)
                            .into(itemIMG)

            }


        }


    }

    fun getScreenWith() : Int {

         var  wm : WindowManager=  context.getSystemService(Context.WINDOW_SERVICE) as WindowManager

            var display  = wm.defaultDisplay

            var size = Point()

            display.getSize(size)

            return size.y

    }


}