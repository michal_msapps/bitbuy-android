package com.msapps.bitbuy.enums;

import android.app.Fragment;

import com.msapps.bitbuy.fragments.AddEditFragment;
import com.msapps.bitbuy.fragments.ItemFragment;
import com.msapps.bitbuy.fragments.MainGridFragment;
import com.msapps.bitbuy.fragments.MyCandidacyFragment;
import com.msapps.bitbuy.fragments.NotoficationFragment;
import com.msapps.bitbuy.fragments.ProfileFrgment;
import com.msapps.bitbuy.fragments.UserItemsFragment;

public enum FragmentName {
    MAIN_GRID_FRAGMENT("Search"),
    USER_ITEM_FRAGMENT("My Items"),
    ADD_ITEM_FRAGMENT("Add Item"),
    NOTIFICATION_FRAGMENT("Notifications"),
    CANDIDACY_FRAGMENT("My Candidacy"),
    PROFILE_FRAGMENT("Profile"),
    ITEM_FRAGMENT("Item Fragment")
    ;


    private final String name;





    public static FragmentName getValue(String fragmentName){
        switch (fragmentName){
            case "Search":
                return MAIN_GRID_FRAGMENT;
            case "My Items":
                return USER_ITEM_FRAGMENT;
            case "Add Item":
                return ADD_ITEM_FRAGMENT;
            case "Notifications":
                return NOTIFICATION_FRAGMENT;
            case "My Candidacy":
                return CANDIDACY_FRAGMENT;
            case "Profile":
                return PROFILE_FRAGMENT;
            case "Item Fragment":
                return ITEM_FRAGMENT;
                default:
                    return null;
        }
    }

    public static Fragment getFragment(FragmentName fragmentName){


        switch (fragmentName){
            case MAIN_GRID_FRAGMENT:
                return new MainGridFragment();
            case USER_ITEM_FRAGMENT:
                return new UserItemsFragment();
            case ADD_ITEM_FRAGMENT:
              return new AddEditFragment();
            case NOTIFICATION_FRAGMENT:
                return new NotoficationFragment();
            case CANDIDACY_FRAGMENT:
                return new MyCandidacyFragment();
            case PROFILE_FRAGMENT:
                return new ProfileFrgment();
            case ITEM_FRAGMENT:
                return new ItemFragment();
                default:
                    return null;
        }
    }

    FragmentName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
